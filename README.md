### Description

Writing, reading and editing a theory paper can be difficult, with many dependencies to keep track of. This extension aims to make this process a little easier, by drawing a graph of the dependencies in your paper.

You can also use the graph to navigate the paper.
<p align="center">
<img align="center" src="./bigExample.png" width="95%">
</p>
 This extension adds the graph view in the rightmost panel.
 The PDF preview comes from the [VSCode Latex workshop extension](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop). 

### Build and Installation
1. Download:
  Clone this repo into, say, ```~/vsCodeLatexGrapher```.

2. Build:
    ```
    cd ~/vsCodeLatexGrapher
    vsce package --yarn
    ```
3. Install:

    ```
    code --install-extension "./vscodeLatexGrapher-0.0.1.vsix
    cp -v ~/vscodeLatexGrapher/src/*.js ~/.vscode/extensions/peterlin.vscodeLatexGrapher-0.0.1./src
    ```

4. Test:
   Open ```examples/minimalExample.tex``` in VSCode. Then open the command palette (```Cmd+P```) and run ```View LaTeX Theorem/Proof Graph```.

You should see this:

<p align="center">
<img align="center" src="./exampleOutput.png" width="95%">
</p>