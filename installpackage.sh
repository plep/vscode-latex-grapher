#! /usr/bin/env bash
vsce package --yarn
echo "y"
code --install-extension "./vscodeLatexGrapher-0.0.1.vsix"
cd ~/.vscode/extensions/peterlin.vscodeLatexGrapher-0.0.1
mkdir -p src
cp -v ~/code/vscodeLatexGrapher/src/*.js ./src

