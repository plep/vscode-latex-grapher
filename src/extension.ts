// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import * as fs from "fs";
import * as parser from "./parser";
import * as path from "path";

class Dependency extends vscode.TreeItem {
  constructor(
    public readonly label: string,
    public readonly collapsibleState: vscode.TreeItemCollapsibleState
  ) {
    super(label, collapsibleState);
  }

  iconPath = {
    light: path.join(
      __filename,
      "..",
      "..",
      "resources",
      "light",
      "dependency.svg"
    ),
    dark: path.join(
      __filename,
      "..",
      "..",
      "resources",
      "dark",
      "dependency.svg"
    ),
  };
}

export class LatexGraphProvider implements vscode.TreeDataProvider<Dependency> {
  constructor(private workspaceRoot: string) {}

  getTreeItem(element: Dependency): vscode.TreeItem {
    return { label: element.label };
  }

  getTheoremDict(): { [key: string]: parser.ITheorem } {
    const editor = vscode.window.activeTextEditor;
    if (!editor) {
      return {}; // No open text editor
    }
    const { text, auxText } = getSource(editor);
    const theoremDict = parser.theoremsFromSource(text, auxText);
    return theoremDict;
  }

  getChildren(element?: Dependency) {
    if (element !== undefined) {
      const children = this.getTheoremDict()[
        element.label
      ].proof!.dependsOn.map((t) => t.label);
      console.log("children", children);
      const childr = children.map((str) => {
        return new Dependency(str, vscode.TreeItemCollapsibleState.None);
      });
      return Promise.resolve(childr);
    } else {
      const roots = Object.values(this.getTheoremDict())
        .filter((t) => t.usedIn.length === 0)
        .map(
          (t) => new Dependency(t.label, vscode.TreeItemCollapsibleState.None)
        );
      return Promise.resolve(roots);
    }
  }
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  const nodeDependenciesProvider = new LatexGraphProvider(
    vscode.workspace!.rootPath!
  );
  vscode.window.registerTreeDataProvider(
    "latexGraph",
    nodeDependenciesProvider
  );

  const latexWorkshop = vscode.extensions.getExtension(
    "James-Yu.latex-workshop"
  );

  latexWorkshop?.activate();

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with registerCommand
  // The commandId parameter must match the command field in package.json

  vscode.commands.registerCommand(
    "vscodeLatexGrapher.labelreshLatexGraph",
    () => {}
  );

  let disposable = vscode.commands.registerCommand(
    "vscodeLatexGrapher.latexGraph",
    () => {
      // Need this for pdf sync

      const panel = vscode.window.createWebviewPanel(
        "latexGraph", // Identifies the type of the webview. Used internally
        "Graph View", // Title of the panel displayed to the user
        vscode.ViewColumn.Three, // Editor column to show the new webview panel in.
        { enableScripts: true } // Webview options.
      );

      // Get graph data from open editor
      const editor = vscode.window.activeTextEditor;
      if (!editor) {
        vscode.window.showErrorMessage(
          "Could not create graph. Active editor is not a text file."
        );
        return; // No open text editor
      }
      const { text, auxText } = getSource(editor);

      const { graph, theoremDict } = parser.graphFromSource(text, auxText);

      // Set the html

      // URIs for vscode extension to work, cannot import directly
      const pathOf = (file: string) =>
        vscode.Uri.file(path.join(context.extensionPath, "src", file));
      const srcOf = (file: string) => panel.webview.asWebviewUri(pathOf(file));

      panel.webview.html = getWebViewContent(
        srcOf("./d3js.js"),
        srcOf("./dagre.js")
      );

      // There seems to be some race condition
      setTimeout(() => panel.webview.postMessage(graph), 200);

      // Handle the messages from the webview
      panel.webview.onDidReceiveMessage(
        (message) => {
          switch (message.command) {
            case "nodeClick":
              goToLine(theoremDict[message.label].lineNumber, editor);
          }
        },
        undefined,
        context.subscriptions
      );
    }
  );

  context.subscriptions.push(disposable);
}

/**
 * Change the focus to the specified editor
 */
function changeFocus(editor: vscode.TextEditor) {
  return vscode.window.showTextDocument(editor.document.uri, {
    preview: false,
    viewColumn: editor.viewColumn,
  });
}

function getSource(editor: vscode.TextEditor) {
  const text = editor.document.getText();
  const texFileName = editor.document.fileName.slice(0, -4);
  const auxText = fs.readFileSync(texFileName + ".aux", "utf-8");
  return { text, auxText };
}

/**
 * Select and center a specific line in the editor
 * @param lineNumber
 */
function goToLine(lineNumber: number, editor: vscode.TextEditor | undefined) {
  //  Go to line in source
  const range = editor!.document.lineAt(lineNumber - 1).range;
  editor!.selection = new vscode.Selection(range.start, range.start);
  editor!.revealRange(range, vscode.TextEditorRevealType.InCenter);
  // Go to line in preview PDF

  changeFocus(editor!).then(() => {
    vscode.commands.executeCommand("latex-workshop.synctex");
  });
}

function getWebViewContent(d3: vscode.Uri, dagre: vscode.Uri) {
  return `
<!-- If you want to use this in vscode, you should do the following things:
Add   const vscode = acquireVsCodeApi(); to the start of the script 
Replace the script src with vscode uris
Remove the renderGraph call -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Graph View</title>
</head>
<style>
  body {
  font: 300 14px 'Helvetica Neue', Helvetica;
}

.node rect {
  stroke: #333;
  fill: #fff;
  cursor: pointer;
  user-select: none;
}

.node > * {
  cursor: pointer
}





.node:hover > * > * {
  fill: darkblue;
  text-decoration: underline;
}


.edgePath path {
  stroke: lightblue;
  fill: lightblue;
  stroke-width: 1.5px;
}

.Complete rect {
  fill: #5cd65c;
}

.NeedsWork rect {
  fill: #ff8080;
}
</style>
<body>
   <div id="my_dataviz">
   </div>
</body>
<script src=${d3}></script>
<script src=${dagre}> </script>

<script>
const vscode = acquireVsCodeApi();
  const data = {"nodes":[{"id":"thm:mainthm"},{"id":"lem:firstlemma"},{"id":"lem:secondlemma"}],"links":[{"source":"thm:mainthm","target":"lem:firstlemma"},{"source":"thm:mainthm","target":"lem:secondlemma"}]}
// set the dimensions and margins of the graph
var margin = {top: 10, right: 30, bottom: 30, left: 40},
width = 1500 - margin.left - margin.right,
height = 1500 - margin.top - margin.bottom;

// append the svg object to the body of the page
const svg = d3.select("#my_dataviz")
.append("svg")
.call(d3.zoom().on("zoom", function () {
    svg.attr("transform", d3.event.transform)
 }))
.attr("width", width + margin.left + margin.right)
.attr("height", height + margin.top + margin.bottom)
.on("dblclick.zoom", null)
.append("g")
.attr("transform",
      "translate(" + margin.left + "," + margin.top + ")");



window.addEventListener('message',
  event=>{
    console.log(event.data);
    renderGraph(event.data)
  }
)

// Initialize the links
function renderGraph(data) {
  const g = new dagreD3.graphlib.Graph().setGraph({});
  data.nodes.forEach( (thm)=> g.setNode(thm.id, {label: thm.label, class: thm.data.proofNodeClass ? thm.data.proofNodeClass : theorem.data.thmNodeClass==="NoProof" ? "NeedsWork" : "" }))
  data.links.forEach((link)=> {g.setEdge(link.source, link.target, {})
})
g.nodes().forEach(function(v) {
  var node = g.node(v);
  node.rx = node.ry = 5;
});

g.edges().forEach(function(e) {
  var edge = g.edge(e.v, e.w);
  edge.curve = d3.curveBasis;
});


  const svg = d3.select("svg")
  const inner = svg.select("g");
  const render = new dagreD3.render();
  render(inner, g)
  inner.selectAll("g.node").on('click', function (label) { return passMessage(label) });
}

function passMessage(label) {
  vscode.postMessage({command: 'nodeClick',
    label
})



}


</script>
</html>
`;
}

// this method is called when your extension is deactivated
export function deactivate() {}
