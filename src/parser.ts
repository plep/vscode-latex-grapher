// Parses a latex file and outputs the underlying graph
// structure of the theorems and lemmas.
// Also matches the theorem labelerences to the theorem labels
// using the aux file

interface IProof {
  belongsTo: ITheorem;
  dependsOn: ITheorem[];
  nodeClass?: string;
}

export interface ITheorem {
  label: Theoremlabel;
  usedIn: IProof[];
  proof?: IProof;
  labelName?: string;
  nodeClass?: string;
  lineNumber: number;
}

type Theoremlabel = string;

export interface ILink {
  source: string;
  target: string;
  id?: number;
}

export interface INode {
  id: string;
  label?: string;
  title?: string;
  data: { [key: string]: string | undefined | number };
}

export interface IGraph {
  nodes: INode[];
  links: ILink[];
}

function newProof(belongsTo: ITheorem, nodeClass?: string) {
  const proof = {
    belongsTo,
    dependsOn: [] as ITheorem[],
    nodeClass,
  };
  belongsTo.proof = proof;
  return proof;
}

function newTheorem(thmlabel: Theoremlabel): ITheorem {
  const thm = {
    label: thmlabel,
    lineNumber: 0,
    proof: undefined,
    nodeClass: "NoProof",
    usedIn: [] as IProof[],
  };
  return thm;
}

/**
 * Remove comments and irrelevant text
 * @param source
 */
export function stripTex(source: string) {
  const decomment = source.replace(/%.*?[\r\n]+/gm, "");
  const tokens = decomment.match(
    /\\(begin|end){.+?}(\[.*?\])?|\\label{.*?}|\\label{.*?}|\\ref{.*?}|\\nodeClass{.*?}/gi
  ); // Beginning or end of token
  return tokens;
}

export function getTheoremProofData(
  source: string
): IterableIterator<RegExpMatchArray> {
  const re = /\\begin{(?<envType>theorem|lemma|proof|corollary|proposition)}(?:\[Proof of (?:.*?)\\ref{(?<proofOf>.*?)}\])?(?:\[.*?\])?(?:\\label{(?<label>.*?)})?(?:\\nodeClass{(?<nodeClass>.*?)})?(?<insideText>.*?)\\end{\1}/g;
  return source.matchAll(re);
}

/**
 * Given tex source, returns a list of theorems
 * @param source: TeX source
 * @param auxText: aux file
 */
function parse(
  source: string,
  auxText: string
): {
  theorems: ITheorem[];
  theoremDict: { [key: string]: ITheorem };
  proofs: IProof[];
} {
  const theorems: ITheorem[] = [];
  const proofs: IProof[] = [];
  const theoremDict: { [key: string]: ITheorem } = {};

  const data = getTheoremProofData(stripTex(source)!.join(""));
  let newestTheorem = null;
  let counter = 0;

  const addThm = (label: string, nodeClass: string) => {
    const newThm = newTheorem(label);
    theorems.push(newThm);
    theoremDict[label] = newThm;
    return newThm;
  };

  for (const match of data) {
    let { envType, label, proofOf, nodeClass, insideText } = match.groups!;
    if (["theorem", "lemma", "corollary", "proposition"].includes(envType)) {
      if (label === undefined) {
        console.log(`Warning! unnamed theorem ${insideText}`);
        label = `Unnamed ${counter}`;
      }

      if (theoremDict[label] === undefined) {
        newestTheorem = addThm(label, nodeClass);
      } else {
        newestTheorem = theoremDict[label];
        newestTheorem.nodeClass = nodeClass;
      }
    } else if (["proof"].includes(envType)) {
      let proof: IProof;
      if (proofOf !== undefined) {
        if (theoremDict[proofOf] === undefined) {
          throw new Error(
            `Proof of ${proofOf} appeared before the theorem statement.`
          );
        }
        proof = newProof(theoremDict[proofOf], nodeClass);
      } else if (newestTheorem !== null && newestTheorem !== undefined) {
        proof = newProof(newestTheorem, nodeClass);
      } else {
        throw new Error("The proof does not belong to any theorem");
      }
      const refs = insideText.matchAll(/\\ref{(?<ref>.*?)}/g);

      for (const refMatch of refs) {
        const ref = refMatch.groups!.ref;

        if (!["item", "def:", "fig:"].includes(ref.slice(0, 4))) {
          if (theoremDict[ref] === undefined) {
            addThm(ref, nodeClass);
          }
          proof.dependsOn.push(theoremDict[ref]);
        }
      }
    }
  }
  cleanup(theorems, proofs);
  setLineNumbers(source, theoremDict);
  const labelNames = parseAux(auxText);

  for (const entry of theorems) {
    theoremDict[entry.label] = entry;
    theoremDict[entry.label].labelName = labelNames[entry.label];
  }

  return { theorems, theoremDict, proofs };
}

function cleanup(theorems: ITheorem[], proofs: IProof[]) {
  const theoremlabels = theorems.map((thm) => thm.label);

  for (const proof of proofs) {
    removeNonTheorems(proof, theoremlabels);
    updateUsedIn(proof);
    proof.belongsTo.proof = proof;
  }
}

/**
 * Update the usedIn property of the theorems that are called
 * @param proof
 */
function updateUsedIn(proof: IProof) {
  for (const theorem of proof.dependsOn) {
    theorem.usedIn.push(proof);
  }
}

/**
 * Remove labelerences to non-theorems
 * @param proof
 * @param theoremlabels
 */
function removeNonTheorems(proof: IProof, theoremlabels: Theoremlabel[]) {
  proof.dependsOn = proof.dependsOn.filter((thm) =>
    theoremlabels.includes(thm.label)
  );
}

/**
 * Parses an LaTeX aux file to find the labels correponding to the labels
 * @param text The aux file
 */
function parseAux(text: string) {
  const re = /\\newlabel{(?<labelname>.*?)}{{.*?}{.*?}{.*?}{(?<label>.*?)}{.*?}}/gm;
  const result: { [key: string]: string } = {};
  const allMatches = matchAll(text, re);
  for (const match of allMatches) {
    result[match[1]] = match[2];
  }
  return result;
}

function* matchAll(str: string, regexp: RegExp) {
  const flags = regexp.global ? regexp.flags : regexp.flags + "g";
  const re = new RegExp(regexp, flags);
  let match;
  while ((match = re.exec(str))) {
    yield match;
  }
}

function constructGraph(theorems: ITheorem[]): IGraph {
  const nodes: INode[] = [];
  const links: ILink[] = [];
  for (const theorem of theorems) {
    const node: INode = {
      id: theorem.label,
      label: wrapLabel(theorem.label, 12),
      title: theorem.labelName,
      data: {
        thmNodeClass: theorem.nodeClass,
        proofNodeClass: theorem.proof?.nodeClass,
        lineNumber: theorem.lineNumber,
      },
    };
    nodes.push(node);
    if (theorem.proof !== undefined) {
      for (const dependent of theorem.proof.dependsOn) {
        const link: ILink = { source: node.id, target: dependent.label };
        links.push(link);
      }
    }
  }
  const graph = { nodes, links };
  return graph;
}

export function graphFromSource(source: string, auxText: string) {
  const { theorems, theoremDict } = parse(source, auxText);
  const graph = constructGraph(theorems);
  return { graph, theoremDict };
}

export function theoremsFromSource(
  source: string,
  auxText: string
): { [key: string]: ITheorem } {
  return parse(source, auxText).theoremDict;
}

/**
 * Build an index of the line number of each \label
 * @param source Latex source
 */
function setLineNumbers(
  source: string,
  theoremDict: { [key: string]: ITheorem }
): void {
  const lines = source.split("\n");
  let lineNumber = 0;
  for (const line of lines) {
    lineNumber++;
    const labelMatches = line.match(/\\label{(.*?)}/);
    if (labelMatches !== null) {
      const label = labelMatches[1];
      if (theoremDict[label] !== undefined) {
        theoremDict[label].lineNumber = lineNumber;
      }
    }
  }
}

/**
 * Adds linebreaks to a string which is delimited by underscores
 * @param label
 * @param width The approximate width (it is a lower bound for the width)
 */
function wrapLabel(label: string, width: number): string {
  const subStrings = label.split(/[\s_,-]+/);
  let finalString = "";
  let currentLine = "";
  for (const subString of subStrings) {
    if (currentLine.length + subString.length < width) {
      currentLine += subString + "_";
    } else {
      currentLine += subString;
      finalString += currentLine + "\n";
      currentLine = "";
    }
  }
  finalString += currentLine + "\n";
  return finalString.slice(0, -2);
  // return label.replace("_", "s").replace(/([^\n]{1,12})\s/g, "[$1]\n");
}
